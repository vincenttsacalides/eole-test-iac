resource "opennebula_virtual_machine" "k3d_vm" {
  name              = var.one_k3d_vm_name
  template_id       = var.one_k3d_template_id
}

resource "opennebula_virtual_machine" "robotframework_vm" {
  name              = var.one_robot_vm_name
  template_id       = var.one_robot_template_id
  nic {
    network_id      = var.one_robot_network_id
  }
}
