variable "one_k3d_vm_name" {
    description     = "The name of the k3d virtual machine"
    default         = "vm-k3d-eole-test"
}

variable "one_k3d_template_id" {
    description     = "The model id used for create the eole k3d vm"
    default         = "125865"
}

variable "one_robot_vm_name" {
    description     = "The name of the robot virtual machine"
    default         = "vm-robotframework-eole-test"
}

variable "one_robot_template_id" {
    description     = "The model id used for create the robot framework vm"
    default         = "171018"
}

variable "one_robot_network_id" {
    description     = "The network id to use on the robot virtual machine"
    default         = "3521"
}
