# Terraform command explanation / cheatsheet

## Set up the var env
Our terraform iac use three environment variable for the authentication on the open nebula api:
- the api url
- the username to use
- the password matching with the username
```
# The API URL
$ export TF_VAR_api_url="<API_URL_TO_SET>"

# The API username
$ export TF_VAR_api_username="<USERNAME_TO_SET>"

# The API password
$ export TF_VAR_api_password="<PASSWORD_TO_SET>"
```

## Initiate the terraform provider (open nebula)
```
$ terraform init
```

## Validate the terraform syntax on the project file (optional)
```
terraform validate
```

## Visualize the change that the deploy will do
```
terraform plan
```

## Apply the change
```
$ terraform apply --auto-approve # auto approve is to run without say yes another time
```

## Destroy the deployed configuration
```
$ terraform destroy
```
